FROM golang:latest

ENV GOPROXY https://goproxy.cn,direct
WORKDIR $GOPATH/src/djk/gin-blog
COPY . $GOPATH/src/djk/gin-blog
RUN go build .

EXPOSE 8000
ENTRYPOINT ["./gin-blog"]