package main

// import (
// 	"fmt"
// 	"gin-blog/pkg/setting"
// 	"net/http"

// 	"github.com/gin-gonic/gin"
// )

// func main() {
// 	router := gin.Default()
// 	router.GET("/test", func(c *gin.Context) {
// 		c.JSON(200, gin.H{
// 			"message": "test",
// 		})
// 	})

// 	s := &http.Server{
// 		Addr:           fmt.Sprintf(":%d", setting.HTTPPort),
// 		Handler:        router,
// 		ReadTimeout:    setting.ReadTimeout,
// 		WriteTimeout:   setting.WriteTimeout,
// 		MaxHeaderBytes: 1 << 20,
// 	}

// 	s.ListenAndServe()
// }

import (
	"fmt"
	"syscall"

	"gin-blog/models"
	"gin-blog/pkg/gredis"
	"gin-blog/pkg/logging"
	"gin-blog/pkg/setting"
	"gin-blog/routers"

	"github.com/fvbock/endless"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {

	setting.Setup()
	models.Setup()
	logging.Setup()
	gredis.Setup()

	endless.DefaultReadTimeOut = setting.ServerSetting.ReadTimeout
	endless.DefaultWriteTimeOut = setting.ServerSetting.WriteTimeout
	endless.DefaultMaxHeaderBytes = 1 << 20
	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)

	server := endless.NewServer(endPoint, routers.InitRouter())
	server.BeforeBegin = func(add string) {
		logging.Info("Actual pid is %d", syscall.Getpid())
	}

	err := server.ListenAndServe()
	if err != nil {
		logging.Error("Server err: %v", err)
	}
	// router := routers.InitRouter()

	// s := &http.Server{
	// 	Addr:           fmt.Sprintf(":%d", setting.ServerSetting.HttpPort),
	// 	Handler:        router,
	// 	ReadTimeout:    setting.ServerSetting.ReadTimeout,
	// 	WriteTimeout:   setting.ServerSetting.WriteTimeout,
	// 	MaxHeaderBytes: 1 << 20,
	// }

	// s.ListenAndServe()
}
